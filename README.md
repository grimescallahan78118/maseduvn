# Mas.edu.vn

Với những thông tin, kiến thức bổ ích tại [mas.edu.vn](https://mas.edu.vn/) sẽ giúp ích cho bạn và mọi người trong việc chọn lọc thông tin nhanh nhất, chính xác nhất trong thời đại công nghệ số

- Địa chỉ: Tầng 1, số 61 Mễ Trì Thượng, Phường Mễ Trì, Quận Nam Từ Liêm, TP Hà Nội

- SĐT: 0869377999

Năm 2023 Là Năm Con Gì?
Theo âm lịch, năm 2023 được biết tới là năm Quý Mão, tức thị năm con Mèo – con giáp xếp vị trí thứ tư trong tổng 12 con giáp. Theo lịch này, năm Quý Mão sẽ tính bắt đầu từ ngày 22/01/2023 và kết thúc năm Quý Mão vào ngày 09/02/2024 tương ứng theo lịch dương.

Theo lịch vạn niên thì năm 2023 là năm mang thiên can Quý và địa chi Mão. Cụ thể sách chỉ ra rằng:

Thiên can Quý cân xứng mang can Mậu và tương hình có can Đinh và can Kỷ.
Địa chi Mão mang tam hợp bao gồm: Mão – Mùi – Hợi và sở hữu tứ hành xung mang địa chi này là Tý – Mão – Ngọ – Dậu.

https://vimeo.com/user205505711

https://twitter.com/maseduvn

https://500px.com/p/maseduvn
